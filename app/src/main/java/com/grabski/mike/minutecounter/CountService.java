package com.grabski.mike.minutecounter;

import android.app.IntentService;
import android.app.Notification;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

/**
 * Created by mike on 1/31/18.
 */

public class CountService extends IntentService {

    private static final String TAG = CountService.class.getSimpleName();

    public CountService() {
        super(CountService.class.getSimpleName());
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        //nothing to do here
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //Since android O background services have limitations, in this case just display a notification for a second.
        if (Build.VERSION.SDK_INT >= 26) {
            startForeground(1, new Notification());
        }
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());

        Constants.State state = Constants.State.values()[sharedPreferences.getInt(Constants.EXTRA_PROCESS_STATE, 0)];
        //Shouldn't happen but why not check just in case.
        if (state != Constants.State.STARTED) {
            stopSelf();
            return;
        }

        int count = sharedPreferences.getInt(Constants.EXTRA_MINUTE_COUNT, 0);
        sharedPreferences.edit().putInt(Constants.EXTRA_MINUTE_COUNT, ++count).commit();

        //sends a broadcast event indicating that a minute i updated. If activity is registered, will display the change.
        Intent updateActivity = new Intent(Constants.KEY_MINUTE_UPDATED);
        updateActivity.putExtra(Constants.EXTRA_MINUTE_COUNT, count);
        LocalBroadcastManager.getInstance(getApplicationContext()).sendBroadcast(updateActivity);

        Log.e(TAG, "onCreate: count = " + count);
        //kill service since is no longer needed
        stopSelf();
    }

}
