package com.grabski.mike.minutecounter;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;

import com.grabski.mike.minutecounter.receivers.AlarmReceiver;

/**
 * Created by mike on 2/1/18.
 */

public class Utils {

    // Setup a recurring alarm every minute
    public static void scheduleAlarm(Context context) {
        AlarmManager alarmMgr = (AlarmManager) context.getSystemService(Service.ALARM_SERVICE);
        PendingIntent alarmIntent = getAlarmIntent(context);
        if (Build.VERSION.SDK_INT >= 23){
            alarmMgr.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP,System.currentTimeMillis() + 1000 * 60, alarmIntent);
        } else {
            alarmMgr.setExact(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 1000 * 60, alarmIntent);
        }
    }

    private static PendingIntent getAlarmIntent(Context context) {
        Intent intent = new Intent(context, AlarmReceiver.class);
        return PendingIntent.getBroadcast(context, 0, intent, 0);
    }

    public static void cancelAlarms(Context context) {
        ((AlarmManager) context.getSystemService(Service.ALARM_SERVICE)).cancel(getAlarmIntent(context));
    }
}
