package com.grabski.mike.minutecounter;

/**
 * Created by mike on 1/31/18.
 */

public class Constants {

    public static final String EXTRA_MINUTE_COUNT = "minute_count";
    public static final String EXTRA_PROCESS_STATE = "process_state";

    public static final String KEY_MINUTE_UPDATED = "key_minute_updated";

    public enum State{
        WAITING,
        STARTED,
        CLOSED
    }
}
