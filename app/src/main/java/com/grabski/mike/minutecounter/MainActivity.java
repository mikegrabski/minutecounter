package com.grabski.mike.minutecounter;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

import com.grabski.mike.minutecounter.receivers.AlarmReceiver;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView datePrinterTextView;
    private TextView minuteCountTextView;
    private TextView statusTextView;

    private SharedPreferences sharedPreferences;
    private BroadcastReceiver minuteReceiver;

    private DatePrinter datePrinter;
    private Constants.State currentState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        datePrinter = new DatePrinter();

        minuteReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                minuteCountTextView.setText(getString(R.string.txt_minutes_counted) + intent.getIntExtra(Constants.EXTRA_MINUTE_COUNT, 0));
            }
        };

        setContentView(R.layout.activity_main);
        findViewById(R.id.btn_start).setOnClickListener(this);
        findViewById(R.id.btn_status).setOnClickListener(this);
        findViewById(R.id.btn_stop).setOnClickListener(this);
        datePrinterTextView = findViewById(R.id.text_date_printer);
        minuteCountTextView = findViewById(R.id.text_minute_count);
        statusTextView = findViewById(R.id.text_status);

        currentState = Constants.State.values()[sharedPreferences.getInt(Constants.EXTRA_PROCESS_STATE, 0)]; //0 is the WAITING state, ordinal ordering
        minuteCountTextView.setText(getString(R.string.txt_minutes_counted) + sharedPreferences.getInt(Constants.EXTRA_MINUTE_COUNT, 0));
        updateStatusText();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_start: {
                startProcesses();
                break;
            }
            case R.id.btn_stop: {
                stopProcesses();
                break;
            }
            case R.id.btn_status: {
                switch (currentState) {
                    case CLOSED:
                        currentState = Constants.State.WAITING;//didn't understand task? waiting ?= Closed
                        updateStatusText();
                        break;
                    case WAITING:
                        break;
                    case STARTED:
                        stopProcesses();
                        break;
                }
            }

        }
    }

    private void startProcesses() {
        datePrinter.startTimer(datePrinterTextView);
        if (currentState == Constants.State.STARTED)
            return;
        Utils.scheduleAlarm(getApplicationContext());
        currentState = Constants.State.STARTED;
        sharedPreferences.edit().putInt(Constants.EXTRA_MINUTE_COUNT, 0)
                .putInt(Constants.EXTRA_PROCESS_STATE, currentState.ordinal()).apply();
        updateStatusText();
    }

    private void stopProcesses() {
        if (currentState == Constants.State.CLOSED || currentState == Constants.State.WAITING)
            return;
        datePrinter.stopTimer();
        currentState = Constants.State.CLOSED;
        Utils.cancelAlarms(getApplicationContext());
        sharedPreferences.edit().putInt(Constants.EXTRA_MINUTE_COUNT, 0)
                .putInt(Constants.EXTRA_PROCESS_STATE, currentState.ordinal()).apply();
        minuteCountTextView.setText(getString(R.string.txt_minutes_counted) + 0);
        updateStatusText();
    }

    private void updateStatusText() {
        statusTextView.setText(getString(R.string.txt_status) + currentState.name());
    }

    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(getApplicationContext()).registerReceiver(minuteReceiver, new IntentFilter(Constants.KEY_MINUTE_UPDATED));
    }

    @Override
    protected void onStop() {
        //unregister receiver for safety reasons. Not in onDestroy because onDestroy is not reliably always called
        LocalBroadcastManager.getInstance(getApplicationContext()).unregisterReceiver(minuteReceiver);
        super.onStop();
    }

    @Override
    protected void onPause() {
        datePrinter.stopTimer();
        super.onPause();
    }

}
