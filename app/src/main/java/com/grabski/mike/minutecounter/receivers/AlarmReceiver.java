package com.grabski.mike.minutecounter.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.grabski.mike.minutecounter.CountService;
import com.grabski.mike.minutecounter.Utils;

/**
 * Created by mike on 1/31/18.
 */

public class AlarmReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        //We could increment minutes here, but as the task wanted a service... Start a service and increment there
        Intent minuteUpdater = new Intent(context, CountService.class);
        ContextCompat.startForegroundService(context, minuteUpdater);
        Utils.scheduleAlarm(context.getApplicationContext());
        Log.d(AlarmReceiver.class.getSimpleName(), "Called context.startService from AlarmReceiver.onReceive");
    }
}
