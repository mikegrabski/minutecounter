package com.grabski.mike.minutecounter;

import android.util.Log;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by mike on 1/1/18.
 * ============================
 * deals with 5 second interval and printing on a textView
 */

public class DatePrinter {

    private static final String TAG = DatePrinter.class.getSimpleName();

    private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss.SSS");

    private static final int INTERVAL = 5000;

    private final Object syncer = new Object();

    private final Runnable timerRunnable = new Runnable() {
        @Override
        public void run() {
            synchronized (syncer) {
                while (shouldCount) {
                    try {
                        print();
                        //waits 5 seconds to print time again, or until is interrupted by notify()
                        syncer.wait(INTERVAL);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            System.out.println("stopping");
        }
    };

    private boolean shouldCount;
    private TextView textView;

    public DatePrinter() {
        shouldCount = false;
    }

    /**
     * Starts the timer thread.
     *
     * @param textView on which to output the date.
     */
    public void startTimer(TextView textView) {
        this.textView = textView;
        startTimer();
    }

    public void startTimer() {
        if (shouldCount) {
            Log.w(TAG, "Thread already started! Ignoring.");
            return;
        }
        shouldCount = true;
        new Thread(timerRunnable).start();
    }

    public void stopTimer() {
        synchronized (syncer) {
            shouldCount = false;
            syncer.notify();
            //notifies the object to wake up from wait() and get out of the loop
        }
    }

    /**
     * Prints the current date to the log and a textView, if available
     */
    private void print() {
        final String formattedDate = simpleDateFormat.format(new Date(System.currentTimeMillis()));
        Log.i(TAG, formattedDate);
        if (textView != null) {
            textView.post(new Runnable() {
                @Override
                public void run() {
                    textView.setText(formattedDate);
                }
            });
        }
    }
}