package com.grabski.mike.minutecounter.receivers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.preference.PreferenceManager;

import com.grabski.mike.minutecounter.Constants;
import com.grabski.mike.minutecounter.Utils;

/**
 * Created by mike on 1/31/18.
 */

public class BootReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Constants.State state = Constants.State.values()[PreferenceManager.getDefaultSharedPreferences(context).getInt(Constants.EXTRA_PROCESS_STATE,0)];
        if (state == Constants.State.STARTED) {
            Utils.scheduleAlarm(context.getApplicationContext());
        }
    }

}
